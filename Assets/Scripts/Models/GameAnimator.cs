﻿using System;
using System.Collections.Generic;

namespace ZiMAD.Match3.Model
{
    public class GameAnimator
    {
        private bool started;
        private bool complete;
        public event Action StartEvent;
        public event Action CompleteEvent;
        private Queue<AnimationContext> animationQueue = new Queue<AnimationContext>();


        public void AddAnimation(AnimationContext animationContext)
        {
            animationContext.onComplete += AnimateNext; // auto remove then animationContext dispose
            animationQueue.Enqueue(animationContext);
            if (!started)
            {
                complete = false;
                started = true;
                StartAnimate();
            }
        }

        private void StartAnimate()
        {
            if (StartEvent != null)
            {
                StartEvent();
            }
            AnimateNext();
        }

        private void AnimateNext()
        {
            if (animationQueue.Count == 0)
            {
                CompleteAnimate();
                return;
            }
            animationQueue.Dequeue().Play();
        }

        private void CompleteAnimate()
        {
            started = false;
            complete = true;
            if (CompleteEvent != null)
            {
                CompleteEvent();
            }
        }
    }
}