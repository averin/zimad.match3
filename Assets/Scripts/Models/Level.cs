﻿using System;
using ZiMAD.Match3.Core;
using LevelConfiguration = ZiMAD.Match3.Core.LevelConfiguration;
using Mission = ZiMAD.Match3.Core.Mission;
using Grid = ZiMAD.Match3.Core.Grid;

namespace ZiMAD.Match3.Model
{
    public class Level : Grid
    {
        public event Action MissionCompleteEvent;
        public event Action StepCompleteEvent = () => { };
        public event Action<CellIndex, CellIndex> SwapSuccessEvent = (from, to) => { };

        private Mission mission { get; set; }

        public Level(LevelConfiguration configuration, int seed) : base(configuration.Width, configuration.Height, seed)
        {
            //ColorsWasCollectedEvent += (indexes) => mission.Collect(indexes.Count);
        }

        public void Initialize()
        {
            InitializeNeighbors();
            InitializeCellColor();
            StepCompleteEvent();
        }

        public void SwapGems(CellIndex from, CellIndex to)
        {
            if (CanSwap(from, to))
            {
                SwapSuccessEvent(from, to);
                CheckMatch();
                if (IsMatch)
                {
                    Collect();
                    StepCompleteEvent();
                }
                do
                {
                    while (!IsFilled)
                    {
                        FillStep();
                        StepCompleteEvent();
                    }
                    CheckMatch();
                    if (IsMatch)
                    {
                        Collect();
                        StepCompleteEvent();
                    }
                } while (!IsFilled);
            }
        }
    }
}