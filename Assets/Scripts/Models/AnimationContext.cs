﻿using System;
using System.Collections.Generic;
using ZiMAD.Match3.Core;

namespace ZiMAD.Match3.Model
{
    public class AnimationContext
    {
        private List<Func<IGameAnimation>> animationExtractors = new List<Func<IGameAnimation>>();
        public event Action onComplete;
        private int animationInProgressCount = 0;

        public void AddExtractor(Func<IGameAnimation> extructorFunc)
        {
            animationExtractors.Add(extructorFunc);
        }

        public void Play()
        {
            animationInProgressCount = animationExtractors.Count;
            if (animationInProgressCount == 0)
            {
                onComplete();
                return;
            }
            animationExtractors.ForEach(extructor =>
            {
                var animation = extructor();
                animation.FullCompleteEvent += onAnimationComplete;
                animation.PlayAnimation();
            });
        }

        public void onAnimationComplete(IGameAnimation animation)
        {
            animation.FullCompleteEvent -= onAnimationComplete;
            animationInProgressCount--;
            if (animationInProgressCount < 1)
            {
                onComplete();
            }
        }
    }
}