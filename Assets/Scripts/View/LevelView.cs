﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using GemColor = ZiMAD.Match3.Core.GemColor;
using CellIndex = ZiMAD.Match3.Core.CellIndex;
using IGameAnimation = ZiMAD.Match3.Core.IGameAnimation;

namespace ZiMAD.Match3.View
{
    public class LevelView : MonoBehaviour
    {
        // TODO: Custom editor need
        [Serializable]
        public class Colors
        {
            public GemColor gemColor;
            public Sprite sprite;
        }

        [SerializeField] private List<Colors> colors;
        [SerializeField] private GemView gemViewPrefab;
        private Dictionary<CellIndex, GemView> gems = new Dictionary<CellIndex, GemView>();
        private Stack<GemView> unattachedGems = new Stack<GemView>();

        public GemView CreateGemView(GemColor color, CellIndex index)
        {
            GemView gem = unattachedGems.Count == 0 ? Instantiate(gemViewPrefab) : unattachedGems.Pop();
            gem.name = string.Format("Gem {0} {1}", index, color);
            gem.SetSprite(colors.First(x => x.gemColor == color).sprite);
            gem.GemColor = color;
            return gem;
        }

        public GemView Get(CellIndex index)
        {
            return gems[index];
        }

        public void Swap(CellIndex from, CellIndex to)
        {
            var fromGem = gems[from];
            gems[from] = gems[to];
            gems[to] = fromGem;
            gems[from].name = string.Format("Gem {0} {1}", from, gems[from].GemColor);
            gems[to].name = string.Format("Gem {0} {1}", to, gems[to].GemColor);
        }

        public void Drop(CellIndex from, CellIndex to)
        {
            gems[to] = gems[from];
            gems[to].name = string.Format("Gem {0} {1}", to, gems[to].GemColor);
            gems[from] = null;
        }

        public void AddGem(CellIndex index, GemView gem)
        {
            gems[index] = gem;
        }

        public void RemoveGem(CellIndex index)
        {
            var gem = gems[index];
            var animations = gem.GetComponents<IGameAnimation>();
            var animationsInProgress = new List<IGameAnimation>();
            foreach (var gameAnimation in animations)
            {
                if (gameAnimation.InProgress)
                {
                    animationsInProgress.Add(gameAnimation);
                }
            }
            var animationsInProgressCount = animationsInProgress.Count();
            if (animationsInProgressCount == 0)
            {
                gem.name = "unattached gem";
                unattachedGems.Push(gem);
            }
            Action<IGameAnimation> unattach = null;
            unattach = (x) =>
            {
                x.CompleteEvent -= unattach;
                animationsInProgressCount--;
                if (animationsInProgressCount == 0)
                {
                    gem.name = "unattached gem";
                    unattachedGems.Push(gem);
                }
            };
            foreach (var animation in animationsInProgress)
            {
                animation.CompleteEvent += unattach;
            }
            gems[index] = null;
        }
    }
}