﻿using System;
using System.Linq;
using UnityEngine;
using UnityEngine.EventSystems;
using CellIndex = ZiMAD.Match3.Core.CellIndex;
using Grid = UnityEngine.Grid;

namespace ZiMAD.Match3.View
{
    public class GameFieldView : MonoBehaviour, IPointerClickHandler
    {
        private static readonly float pixelsPerUnit = 100;

        [SerializeField] private Grid grid;
        [SerializeField] private BoxCollider2D collider2D;
        [SerializeField] private SpriteRenderer slots;

        private Texture2D maskTexture;
        private Vector2 sizeInUnits;
        private int width;
        private int height;

        private void OnDestroy()
        {
            if (maskTexture != null)
            {
                Destroy(maskTexture);
            }
        }

        public void PrepareBackground(int width, int height)
        {
            this.width = width;
            this.height = height;
            CalculateSize();
            collider2D.size = sizeInUnits;
            CreateLevelMask();
            CreateSlots();
        }

        public Vector2 GetCellCenterLocal(CellIndex index)
        {
            return (Vector2) grid.GetCellCenterLocal(new Vector3Int(index.X, index.Y, 0)) - sizeInUnits / 2;
        }

        public CellIndex GetCellIndex(PointerEventData data)
        {
            var position = transform.InverseTransformPoint(data.pointerCurrentRaycast.worldPosition);
            var x = Mathf.FloorToInt((position.x + grid.cellSize.x * (width % 2) / 2) / grid.cellSize.x);
            var y = Mathf.FloorToInt((position.y + grid.cellSize.y * (height % 2) / 2) / grid.cellSize.y);
            return new CellIndex(x + width / 2, y + height / 2);
        }

        private void CalculateSize()
        {
            sizeInUnits = grid.GetCellCenterLocal(new Vector3Int(width - 1, height - 1, 0)) + grid.cellSize / 2;
        }

        private void CreateLevelMask()
        {
            var pixelSize = sizeInUnits * pixelsPerUnit;
            var x = (int) pixelSize.x;
            var y = (int) pixelSize.y;
            maskTexture = new Texture2D(x, y);
            Color[] colors = Enumerable.Repeat<Color>(Color.black, x * y).ToArray();
            maskTexture.SetPixels(0, 0, x, y, colors);
            maskTexture.Apply();
            GetComponent<SpriteMask>().sprite = Sprite.Create(maskTexture, new Rect(0, 0, x, y), Vector2.one / 2);
        }

        private void CreateSlots()
        {
            slots.size = sizeInUnits;
        }

        private bool inputEnabled;

        public bool InputEnabled
        {
            get { return inputEnabled; }
            set
            {
                inputEnabled = value;
                Debug.Log("inputEnabled " + value);
                if (!inputEnabled)
                {
                    selected = false;
                }
            }
        }

        public Action<CellIndex, CellIndex> Swap = (from, to) => { };
        private bool selected = false;
        private CellIndex selectedCellIndex;

        public void OnPointerClick(PointerEventData eventData)
        {
            if (!InputEnabled)
            {
                return;
            }

            if (!selected)
            {
                selected = true;
                selectedCellIndex = GetCellIndex(eventData);
                return;
            }
            var index = GetCellIndex(eventData);
            if (selectedCellIndex == index)
            {
                return;
            }
            var deltaX = Mathf.Abs(selectedCellIndex.X - index.X);
            var deltaY = Mathf.Abs(selectedCellIndex.Y - index.Y);
            if (deltaX > 1 || deltaY > 1 || deltaX + deltaY > 1)
            {
                selectedCellIndex = index;
                return;
            }
            selected = false;
            Swap(selectedCellIndex, index);
        }
    }
}