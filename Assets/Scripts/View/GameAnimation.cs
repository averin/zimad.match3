﻿using System;
using System.Collections;
using UnityEngine;
using IGameAnimation = ZiMAD.Match3.Core.IGameAnimation;

namespace ZiMAD.Match3.View
{
    public abstract class GameAnimation : MonoBehaviour, IGameAnimation
    {
        public bool InProgress { get; private set; }

        public event Action<IGameAnimation> CompleteEvent;
        public event Action<IGameAnimation> FullCompleteEvent;

        public void PlayAnimation()
        {
            gameObject.SetActive(true);
            StartCoroutine(PlayAnimationEnumerator());
        }

        private IEnumerator PlayAnimationEnumerator()
        {
            InProgress = true;
            yield return Play();
            InProgress = false;
            Reset();
            if (CompleteEvent != null)
            {
                CompleteEvent(this);
            }
            yield return null;
            if (FullCompleteEvent != null)
            {
                FullCompleteEvent(this);
            }
        }

        protected abstract IEnumerator Play();

        protected virtual void Reset()
        {
        }
    }
}