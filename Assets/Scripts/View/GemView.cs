﻿using UnityEngine;
using GemColor = ZiMAD.Match3.Core.GemColor;

namespace ZiMAD.Match3.View
{
    public class GemView : MonoBehaviour
    {
        [SerializeField] private GemMoveAnimation moveAnimation;
        [SerializeField] private GemCollectAnimation collectAnimation;

        [SerializeField] private SpriteRenderer renderer;

        public GemColor GemColor { get; set; }

        public void SetSprite(Sprite sprite)
        {
            renderer.sprite = sprite;
        }

        public GemMoveAnimation GetMoveAnimation()
        {
            return moveAnimation;
        }

        public GemCollectAnimation GetCollectAnimation()
        {
            return collectAnimation;
        }

        public void SetActive(bool active)
        {
            gameObject.SetActive(active);
        }
    }
}