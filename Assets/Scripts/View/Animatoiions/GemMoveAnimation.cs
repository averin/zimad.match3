﻿using System.Collections;
using UnityEngine;

namespace ZiMAD.Match3.View
{
    public class GemMoveAnimation : GameAnimation
    {
        [SerializeField] private Transform gem;
        [SerializeField] private float animationTime;
        [SerializeField] private float animationFreezeTime = 0.1f;

        private Vector3 startPosition;
        private Vector3 endPosition;

        private float time;

        public void SetPositions(Vector3 startPosition, Vector3 endPosition)
        {
            this.startPosition = startPosition;
            this.endPosition = endPosition;
            time = 0;
        }

        protected override IEnumerator Play()
        {
            gem.localPosition = startPosition;
            while (time < animationTime)
            {
                yield return new WaitForEndOfFrame();
                time += Time.deltaTime;
                gem.localPosition = Vector3.Lerp(startPosition, endPosition, time / animationTime);
            }
            gem.localPosition = endPosition;
            yield return new WaitForSeconds(animationFreezeTime);
        }
    }
}