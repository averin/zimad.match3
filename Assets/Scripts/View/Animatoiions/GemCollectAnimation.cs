﻿using System.Collections;
using UnityEngine;

namespace ZiMAD.Match3.View
{
    public class GemCollectAnimation : GameAnimation
    {
        [SerializeField] private SpriteRenderer gem;

        [SerializeField] private float animationTime;

        private float time;

        protected override IEnumerator Play()
        {
            gem.color = Color.white;
            while (time < animationTime)
            {
                yield return new WaitForEndOfFrame();
                time += Time.deltaTime;
                gem.color = Color.Lerp(Color.white, Color.clear, time / animationTime);
            }
            gem.color = Color.clear;
            yield return null;
        }

        protected override void Reset()
        {
            gem.color = Color.white;
        }
    }
}