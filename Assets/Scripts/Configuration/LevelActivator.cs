﻿using System;
using UnityEngine;
using ZiMAD.Match3.Core;
using ZiMAD.Match3.Model;
using ZiMAD.Match3.View;

namespace ZiMAD.Match3.Activator
{
    public class LevelActivator : MonoBehaviour
    {
        [SerializeField] private LevelView levelView;
        [SerializeField] private GameFieldView gameFieldView;

        private Level level;

        private GameAnimator gameAnimator = new GameAnimator();
        private AnimationContext currentAnimationContext;

        private void Start()
        {
            gameAnimator.StartEvent += () => { gameFieldView.InputEnabled = false; };
            gameAnimator.CompleteEvent += () => { gameFieldView.InputEnabled = true; };
            StartLevel(new LevelConfiguration()
            {
                Width = 10,
                Height = 10,
                TargetGems = 30
            });
        }

        private AnimationContext GetAnimationContext()
        {
            return currentAnimationContext;
        }

        private void ClearAnimationContext()
        {
            currentAnimationContext = new AnimationContext();
        }

        public void StartLevel(LevelConfiguration configuration)
        {
            var seed = DateTime.Now.Millisecond;
            gameFieldView.PrepareBackground(configuration.Width, configuration.Height);
            level = new Level(configuration, seed);
            gameFieldView.Swap = level.SwapGems;
            currentAnimationContext = new AnimationContext();
            level.StepCompleteEvent += () =>
            {
                var context = GetAnimationContext();
                gameAnimator.AddAnimation(context);
                ClearAnimationContext();
            };

            level.ColorWasCreatedEvent += (color, index) =>
            {
                GemView gem = null;
                GetAnimationContext().AddExtractor(() =>
                {
                    gem = levelView.CreateGemView(color, index);
                    var gemCreateAnimation = gem.GetMoveAnimation();
                    var startPosition =
                        gameFieldView.GetCellCenterLocal(new CellIndex(index.X, configuration.Height + index.Y));
                    var endPosition = gameFieldView.GetCellCenterLocal(index);
                    gemCreateAnimation.SetPositions(startPosition, endPosition);
                    Action<IGameAnimation> animationOnCompleteSelfHandler = null;

                    animationOnCompleteSelfHandler = (selfAnimation) =>
                    {
                        selfAnimation.CompleteEvent -= animationOnCompleteSelfHandler;
                        levelView.AddGem(index, gem);
                    };
                    gemCreateAnimation.CompleteEvent += animationOnCompleteSelfHandler;
                    return gemCreateAnimation;
                });
            };

            level.ColorsWasCollectedEvent += indexes =>
            {
                foreach (var index in indexes)
                {
                    var cachedIndex = index;

                    GetAnimationContext().AddExtractor(() =>
                    {
                        var gem = levelView.Get(cachedIndex);
                        var animation = gem.GetCollectAnimation();
                        Action<IGameAnimation> animationOnCompleteSelfHandler = null;
                        animationOnCompleteSelfHandler = (selfAnimation) =>
                        {
                            selfAnimation.CompleteEvent -= animationOnCompleteSelfHandler;
                            gem.transform.localPosition = gameFieldView.GetCellCenterLocal(new CellIndex(-1, -1));
                            levelView.RemoveGem(index);
                        };
                        animation.CompleteEvent += animationOnCompleteSelfHandler;
                        return animation;
                    });
                }
            };

            level.SwapSuccessEvent += (from, to) =>
            {
                AnimationContext animationContext = new AnimationContext();
                animationContext.AddExtractor(() =>
                {
                    var fromGem = levelView.Get(from);
                    var fromMoveAnimation = fromGem.GetMoveAnimation();
                    var startFromPosition = gameFieldView.GetCellCenterLocal(from);
                    var endFromPosition = gameFieldView.GetCellCenterLocal(to);
                    fromMoveAnimation.SetPositions(startFromPosition, endFromPosition);
                    return fromMoveAnimation;
                });
                animationContext.AddExtractor(() =>
                {
                    var toGem = levelView.Get(to);
                    var toMoveAnimation = toGem.GetMoveAnimation();
                    var startFromPosition = gameFieldView.GetCellCenterLocal(from);
                    var endFromPosition = gameFieldView.GetCellCenterLocal(to);
                    toMoveAnimation.SetPositions(endFromPosition, startFromPosition);
                    Action<IGameAnimation> animationOnCompleteSelfHandler = null;
                    animationOnCompleteSelfHandler = (selfAnimation) =>
                    {
                        selfAnimation.CompleteEvent -= animationOnCompleteSelfHandler;
                        levelView.Swap(from, to);
                    };
                    toMoveAnimation.CompleteEvent += animationOnCompleteSelfHandler;

                    return toMoveAnimation;
                });
                gameAnimator.AddAnimation(animationContext);
            };

            level.CollorWasDroppedEvent += (from, to) =>
            {
                GetAnimationContext().AddExtractor(() =>
                {
                    var fromGem = levelView.Get(from);
                    var fromMoveAnimation = fromGem.GetMoveAnimation();
                    var startFromPosition = gameFieldView.GetCellCenterLocal(from);
                    var endFromPosition = gameFieldView.GetCellCenterLocal(to);
                    fromMoveAnimation.SetPositions(startFromPosition, endFromPosition);
                    Action<IGameAnimation> animationOnCompleteSelfHandler = null;
                    animationOnCompleteSelfHandler = (selfAnimation) =>
                    {
                        selfAnimation.CompleteEvent -= animationOnCompleteSelfHandler;
                        levelView.Drop(from, to);
                    };
                    fromMoveAnimation.CompleteEvent += animationOnCompleteSelfHandler;
                    return fromMoveAnimation;
                });
            };
            level.Initialize();
        }
    }
}