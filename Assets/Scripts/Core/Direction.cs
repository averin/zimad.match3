﻿using System;

//    2    5    8
//    1    4    7
//    0    3    6

namespace ZiMAD.Match3.Core
{
    [Flags]
    public enum Direction
    {
        LeftDown = 0,
        Left = 1,
        LeftUp = 2,
        Down = 3,
        Up = 5,
        RightDown = 6,
        Right = 7,
        RightUp = 8
    }

    public static class DirectionHelper
    {
        public static int GetSerialNumber(this Direction direction)
        {
            var dir = (int) direction;
            if (dir < 5)
            {
                return dir;
            }
            return dir - 1;
        }

        public static CellIndex Offset(this Direction direction, CellIndex cellIndex)
        {
            var dir = (int) direction;
            return new CellIndex(cellIndex.X + dir / 3 - 1, cellIndex.Y + dir % 3 - 1);
        }

        public static Direction DirectionTo(this CellIndex from, CellIndex to)
        {
            return (Direction) ((to.X - from.X + 1) * 3 + to.Y - from.Y + 1);
        }

        public static int OppositeIndex(this int directionValue)
        {
            return 8 - directionValue;
        }

        public static Direction OppositeDirection(this Direction direction)
        {
            return (Direction) (8 - (int) direction);
        }
    }
}