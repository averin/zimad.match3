﻿using System;
using System.Collections.Generic;
using System.Linq;
using Random = System.Random;

namespace ZiMAD.Match3.Core
{
    public class Grid
    {
        protected bool IsFilled { get; private set; }
        protected bool IsMatch { get; private set; }

        public event Action<List<CellIndex>> ColorsWasCollectedEvent = (indexes) => { };
        public event Action<CellIndex, CellIndex> CollorWasDroppedEvent = (before, after) => { };
        public event Action<GemColor, CellIndex> ColorWasCreatedEvent = (color, index) => { };

        #region Private Fields

        private Cell[,] cells;

        private int[] topGroundedIndexes;

        private int width;
        private int height;

        private int colorsCount = Enum.GetNames(typeof(GemColor)).Length;
        private Random random;

        #endregion


        public Grid(int width, int height, int seed)
        {
            this.width = width;
            this.height = height;
            random = new Random(seed);
            cells = new Cell[width, height];
            topGroundedIndexes = Enumerable.Repeat(height - 1, width).ToArray();
            ForEachCells((cell, column, row) => { cells[column, row] = new Cell(); });
        }

        protected bool CanSwap(CellIndex from, CellIndex to)
        {
            if (!IndexIsValid(from) || !IndexIsValid(to))
            {
                return false; //invalid data;
            }
            var cellFrom = cells[from.X, from.Y];
            var cellTo = cells[to.X, to.Y];
            var colorFrom = cellFrom.PullColor();
            var colorTo = cellTo.PullColor();
            if (!cellFrom.CanMatch(to.DirectionTo(from), colorTo) && !cellTo.CanMatch(from.DirectionTo(to), colorFrom))
            {
                cellFrom.PushColor(colorFrom);
                cellTo.PushColor(colorTo);
                return false;
            }
            cellTo.PushColor(colorFrom);
            cellFrom.PushColor(colorTo);
            return true;
        }

        protected void Collect()
        {
            IsMatch = false;
            List<CellIndex> collected = new List<CellIndex>();
            ForEachCells((cell, column, row) =>
            {
                if (cells[column, row].Matched)
                {
                    cells[column, row].Collect();
                    if (row <= topGroundedIndexes[column])
                    {
                        topGroundedIndexes[column] = row - 1;
                    }
                    var cellIndex = new CellIndex(column, row);
                    collected.Add(cellIndex);
                }
            });
            if (ColorsWasCollectedEvent != null)
            {
                ColorsWasCollectedEvent(collected);
            }
            IsFilled = false;
        }

        protected void CheckMatch()
        {
            ForEachCells((cell, column, row) =>
            {
                cells[column, row].CheckMatch();
                IsMatch |= cells[column, row].Matched;
            });
        }

        protected void FillStep()
        {
            bool filling = false;

            for (int column = 0; column < width; column++)
            {
                if (topGroundedIndexes[column] == height - 1)
                {
                    continue;
                }
                filling = true;
                int topUnfilled = topGroundedIndexes[column] + 1;
                for (int row = topUnfilled; row < height - 1; row++)
                {
                    if (!cells[column, row].Empty)
                    {
                        if (row == topGroundedIndexes[column] + 1)
                        {
                            topGroundedIndexes[column] = row;
                        }
                        continue;
                    }
                    if (cells[column, row + 1].Empty)
                    {
                        continue;
                    }
                    if (row == topGroundedIndexes[column] + 1)
                    {
                        topGroundedIndexes[column] = row;
                    }
                    var color = cells[column, row + 1].PullColor();
                    cells[column, row].PushColor(color);
                    if (CollorWasDroppedEvent != null)
                    {
                        CollorWasDroppedEvent(new CellIndex(column, row + 1), new CellIndex(column, row));
                    }
                }
                var createdColor = GenerateColor();
                cells[column, height - 1].PushColor(createdColor);
                if (height - 2 == topGroundedIndexes[column])
                {
                    topGroundedIndexes[column] = height - 1;
                }
                if (ColorWasCreatedEvent != null)
                {
                    ColorWasCreatedEvent(createdColor, new CellIndex(column, height - 1));
                }
            }
            if (!filling)
            {
                IsFilled = true;
            }
        }


        #region Private Methods

        protected void InitializeNeighbors()
        {
            ForEachCells((cell, column, row) =>
            {
                var cellIndex = new CellIndex(column, row);

                foreach (Direction direction in Enum.GetValues(typeof(Direction)))
                {
                    var neighborIndex = direction.Offset(cellIndex);
                    var neighbor = IndexIsValid(neighborIndex)
                        ? cells[neighborIndex.X, neighborIndex.Y]
                        : Cell.EmptyCell;
                    cells[column, row].SetNeighbor(direction, neighbor);
                }
            });
        }

        protected void InitializeCellColor()
        {
            ForEachCells((cell, column, row) =>
            {
                int left = (column == 0) ? -1 : (int) cells[column - 1, row].GemColor;
                int down = (row == 0) ? -1 : (int) cells[column, row - 1].GemColor;
                int current = GenerateColorIndex();

                if (current == left && cell.CanMatch(Direction.Right, (GemColor) current))
                {
                    current++;
                    current %= colorsCount;
                }
                if (current == down)
                {
                    if (cell.CanMatch(Direction.Up, (GemColor) current))
                    {
                        current++;
                        current %= colorsCount;
                        if (current == left && cell.CanMatch(Direction.Right, (GemColor) current))
                        {
                            current++;
                            current %= colorsCount;
                        }
                    }
                }
                GemColor color = (GemColor) current;
                cell.PushColor((GemColor) current);
                if (ColorWasCreatedEvent != null)
                {
                    ColorWasCreatedEvent(color, new CellIndex(column, row));
                }
            });
        }

        private bool IndexIsValid(CellIndex index)
        {
            return index.X > -1 && index.Y > -1 && index.X < width && index.Y < height;
        }

        private int GenerateColorIndex()
        {
            var index = random.Next(colorsCount);
            return index;
        }

        private GemColor GenerateColor()
        {
            return (GemColor) GenerateColorIndex();
        }

        private void ForEachCells(Action<Cell, int, int> action)
        {
            for (int row = 0; row < height; row++)
            {
                for (int column = 0; column < width; column++)
                {
                    action(cells[column, row], column, row);
                }
            }
        }

        #endregion
    }
}