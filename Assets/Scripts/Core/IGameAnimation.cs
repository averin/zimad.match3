﻿using System;

namespace ZiMAD.Match3.Core
{
    public interface IGameAnimation
    {
        bool InProgress { get; }
        event Action<IGameAnimation> CompleteEvent;
        event Action<IGameAnimation> FullCompleteEvent;
        void PlayAnimation();
    }
}