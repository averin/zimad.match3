﻿using System.Linq;
using UnityEngine;

namespace ZiMAD.Match3.Core
{
    public class Cell
    {
        private static readonly int maxNeighbors = 8; // Enum.GetNames(typeof(Direction)).Length;

        public static Cell EmptyCell = new Cell();

        public bool Empty { get; private set; }
        public bool Matched { get; private set; }

        public GemColor GemColor { get; private set; }
        public bool IsActive { get; private set; }
        public int neighborsCount = 0;

        private Cell[] neighbors = new Cell[maxNeighbors];


        public void SetNeighbor(Direction direction, Cell neighbor)
        {
            neighbors[direction.GetSerialNumber()] = neighbor;
            neighborsCount++;
            IsActive = neighborsCount == maxNeighbors;
        }

        public void PushColor(GemColor gemColor)
        {
            GemColor = gemColor;
            Empty = false;
        }

        public GemColor PullColor()
        {
            Empty = true;
            return GemColor;
        }

        private bool NeighborIsSameColor(int index)
        {
            return NeighborIsSameColor(index, GemColor);
        }

        private bool NeighborIsSameColor(int index, GemColor gemColor)
        {
            return neighbors[index].IsActive && !neighbors[index].Empty && gemColor == neighbors[index].GemColor;
        }

        public void CheckMatch()
        {
            for (int i = 1; i < 4; i = i + 2)
            {
                var direction = (Direction) i;
                var directionSerial = direction.GetSerialNumber();
                var opposite = direction.OppositeDirection();
                var oppositeSerial = opposite.GetSerialNumber();
                if (NeighborIsSameColor(directionSerial) && NeighborIsSameColor(oppositeSerial))
                {
                    Matched = true;
                    neighbors[directionSerial].Matched = true;
                    neighbors[oppositeSerial].Matched = true;
                }
            }
        }

        public bool CanMatch(Direction moveDirection, GemColor color)
        {
            for (int i = 1; i < 8; i = i + 2)
            {
                var direction = (Direction) i;
                var directionSerial = direction.GetSerialNumber();

                if (!NeighborIsSameColor(directionSerial, color))
                {
                    continue;
                }
                var opposite = direction.OppositeDirection();
                if (opposite != direction)
                {
                    if (NeighborIsSameColor(opposite.GetSerialNumber(), color))
                    {
                        return true;
                    }
                }
                var neighborsFromDirection = neighbors[directionSerial];
                if (neighborsFromDirection.NeighborIsSameColor(directionSerial, color))
                {
                    return true;
                }
            }
            return false;
        }

        public void Collect()
        {
            Matched = false;
            Empty = true;
        }
    }
}