﻿namespace ZiMAD.Match3.Core
{
    public enum GemColor
    {
        Azure,
        Blue,
        Green,
        Pink,
        Purple,
        Red
    }
}