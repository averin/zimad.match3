﻿namespace ZiMAD.Match3.Core
{
    public struct CellIndex
    {
        public int X;
        public int Y;

        public CellIndex(int x, int y)
        {
            X = x;
            Y = y;
        }

        public override string ToString()
        {
            return string.Format("[X: {0}, Y: {1}]", X, Y);
        }

        public static bool operator ==(CellIndex left, CellIndex right)
        {
            return left.X == right.X && left.Y == right.Y;
        }

        public static bool operator !=(CellIndex left, CellIndex right)
        {
            return left.X != right.X || left.Y != right.Y;
        }
    }
}