﻿namespace ZiMAD.Match3.Core
{
    public class LevelConfiguration
    {
        public int TargetGems { get; set; } // TODO: swith to mission config
        public int Width { get; set; }
        public int Height { get; set; }
    }
}