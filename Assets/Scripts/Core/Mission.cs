﻿using System;

namespace ZiMAD.Match3.Core
{
    public class Mission
    {
        public Action CompleteHandler { get; private set; }

        public int TargetGemCount { get; private set; }

        public int GemCount { get; private set; }

        public Mission(int targetGemCount, Action completeHandler)
        {
            TargetGemCount = targetGemCount;
            CompleteHandler = completeHandler;
        }

        public void Collect(int gemCount)
        {
            GemCount += gemCount;
            if (GemCount > TargetGemCount)
            {
                if (CompleteHandler != null)
                {
                    CompleteHandler();
                }
            }
        }
    }
}